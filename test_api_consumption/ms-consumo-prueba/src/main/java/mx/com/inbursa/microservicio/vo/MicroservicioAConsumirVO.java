package mx.com.inbursa.microservicio.vo;

import java.io.Serializable;

public class MicroservicioAConsumirVO implements Serializable{
	
	private static final long serialVersionUID = -3394206373652126636L;
	private String uri;
	private String method;
	
	public String getUri() {
		return uri;
	}
	public String getMethod() {
		return method;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	
	
}
