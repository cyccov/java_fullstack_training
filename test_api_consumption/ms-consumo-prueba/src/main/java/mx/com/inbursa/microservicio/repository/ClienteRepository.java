package mx.com.inbursa.microservicio.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import mx.com.inbursa.microservicio.entity.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente,Long> {
	
//	@Query("SELECT nombre FROM Cliente c where c.nombre LIKE %?1")
	//findBy_VARIABLE_ ; revisar documentacion spring / gfi
	List<Cliente> findByNombreContaining(String nombre);
	
	@Query("SELECT nombre FROM Cliente c ")
	List<Cliente> findByAndSort(String nombre, Sort sorting);
}
