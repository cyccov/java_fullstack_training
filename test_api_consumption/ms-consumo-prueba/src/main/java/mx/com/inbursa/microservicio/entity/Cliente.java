package mx.com.inbursa.microservicio.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="clientes")
public class Cliente {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "idclientes")
	private long id;
	private String nombre;
	private String apellido;
	@Column(name="forma_pago")
	private String formaDePago;
	@Column(name="creado_en")
	private LocalDateTime creadoEn;
	@Column(name="editado_en")
	private LocalDateTime editadoEn;
	public long getId() {
		return id;
	}
	public String getNombre() {
		return nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public String getFormaDePago() {
		return formaDePago;
	}
	public LocalDateTime getCreadoEn() {
		return creadoEn;
	}
	public LocalDateTime getEditadoEn() {
		return editadoEn;
	}
	public void setId(long id) {
		this.id = id;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public void setFormaDePago(String formaDePago) {
		this.formaDePago = formaDePago;
	}
	public void setCreadoEn(LocalDateTime creadoEn) {
		this.creadoEn = creadoEn;
	}
	public void setEditadoEn(LocalDateTime editadoEn) {
		this.editadoEn = editadoEn;
	}
	
	
}
