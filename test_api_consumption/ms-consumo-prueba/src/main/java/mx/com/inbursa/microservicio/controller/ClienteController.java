package mx.com.inbursa.microservicio.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.com.inbursa.microservicio.entity.Cliente;
import mx.com.inbursa.microservicio.repository.ClienteRepository;
import mx.com.inbursa.microservicio.response.ResponseHandler;

@RestController
@RequestMapping(value="inbursa/api/")
public class ClienteController {
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@GetMapping(path = "cliente/get")
	public ResponseEntity<Object> getClientes(){
		List<Cliente> clientes = clienteRepository.findAll();
		Map<String, Object> submap = new HashMap();
		submap.put("listaClientes",clientes);
		/**con este metodo trae una respuesta data: {lista: [prop1, prop2...] tipo gfi}*/
		return ResponseHandler.generateResponseWithList("Respuesta exitosa", HttpStatus.OK, submap);		
		
	}
	
	@GetMapping(path = "cliente/get/nombres/{nombre}")
	public ResponseEntity<Object> getAllClientes(@PathVariable(value="nombre") String nombre){
		List<Cliente> clientesOrdenados = clienteRepository.findByNombreContaining(nombre);
//		List<Cliente> clientesOrdenados = clienteRepository.findByAndSort(nombre, Sort.by("nombre"));
		Map<String, Object> submap = new HashMap();
		submap.put("listaClientes",clientesOrdenados);
		return ResponseHandler.generateResponseWithList("Respuesta exitosa", HttpStatus.OK, submap);		
	}
	
	
	@GetMapping("cliente/{idclientes}")
	public ResponseEntity<Object> getClienteById(@PathVariable(value="idclientes") Long id){
		Optional<Cliente> cliente = clienteRepository.findById(id);
		return ResponseHandler.generarRespuestaOK("Respuesta exitosa", cliente);
	}
}
