package mx.com.inbursa.microservicio.response;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponseHandler {
	public static ResponseEntity<Object> generateResponse(String mensaje, HttpStatus status, Object responseObject){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("mensaje", mensaje);
		map.put("status", status);
		map.put("data", responseObject);
		return new ResponseEntity<>(map,status);
	}
	
	public static ResponseEntity<Object> generateResponseWithList(String mensaje, HttpStatus status, Object responseObjectWithList){
		Map<String, Object> map = new HashMap();		
		map.put("mensaje", mensaje);
		map.put("status", status.name());
		map.put("data", responseObjectWithList);
		return new ResponseEntity<>(map, status);
	}
	
	public static ResponseEntity<Object> generarRespuestaOK(String mensaje, Object response){
		Map<String, Object> map = new HashMap();
		map.put("mensaje", mensaje);
		map.put("status", HttpStatus.OK);
		map.put("data", response);
		return new ResponseEntity<>(map,HttpStatus.OK);
	}
}
