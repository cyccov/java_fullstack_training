package mx.com.inbursa.microservicio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsConsumoPruebaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsConsumoPruebaApplication.class, args);
	}

}
