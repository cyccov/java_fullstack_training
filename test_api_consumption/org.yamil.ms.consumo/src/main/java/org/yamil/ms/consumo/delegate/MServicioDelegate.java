package org.yamil.ms.consumo.delegate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.yamil.ms.consumo.vo.MServicioVO;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class MServicioDelegate {
	JsonObject respuesta = null;
	public JsonObject consumirServicio(MServicioVO servicioVO) {
		try {
			URL servicio = new URL(servicioVO.getUri());
			HttpURLConnection conn = (HttpURLConnection) servicio.openConnection();
			conn.setRequestMethod(servicioVO.getMethod());
			conn.setRequestProperty("Content-type", "application/json; charset=UTF-8");
			conn.setRequestProperty("Accept", "application/json");
			conn.getContentEncoding();
			
			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(),"UTF-8"));
			respuesta = new JsonParser().parse(br.readLine()).getAsJsonObject();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return respuesta;
	}
}
