package org.yamil.ms.consumo;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.yamil.ms.consumo.delegate.MServicioDelegate;
import org.yamil.ms.consumo.response.MsConsumoResponse;
import org.yamil.ms.consumo.vo.ClienteVO;
import org.yamil.ms.consumo.vo.MServicioVO;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

public class PruebaConsumoGson {
	public static void main(String[] args) {
		String endListaClientes = "http://localhost:8185/inbursa/api/cliente/1";
		String endListaClientesGet = "http://localhost:8185/inbursa/api/cliente/get";
		ClienteVO vo = new ClienteVO();
		MServicioVO msConsumoVO = new MServicioVO();
		MServicioDelegate msConsumo = new MServicioDelegate();
		Type collectionType = new TypeToken<List<MsConsumoResponse>>(){}.getType();

		GsonBuilder gsonBuilder = new GsonBuilder();
		try {
			System.out.println("CONSUMIENDO API...");				
			msConsumoVO.setUri(endListaClientes);
			msConsumoVO.setMethod("GET");
			JsonObject respuesta = msConsumo.consumirServicio(msConsumoVO);
			List<ClienteVO> listaClientes = new ArrayList();
			Map<String, Object> clienteMap = new Gson().fromJson(respuesta.get("data").getAsJsonObject(), HashMap.class);
			clienteMap.get("nombre");
			Map<String, Object> respMsMap = new Gson().fromJson(respuesta, HashMap.class);
			Map listaClientesMap = (Map)respMsMap.get("data");
			System.out.println(listaClientesMap);
			
			System.out.println("Consumiendo API (lista de clientes)");
			msConsumoVO.setUri(endListaClientesGet);
			msConsumoVO.setMethod("GET");		
			respuesta = msConsumo.consumirServicio(msConsumoVO);
			List<MsConsumoResponse> listaClientesExistentes = new Gson().fromJson(respuesta.get("data").getAsJsonObject().get("listaClientes").getAsJsonArray(), collectionType);
			
			for (MsConsumoResponse respuestaMs : listaClientesExistentes) {
				System.out.println(respuestaMs);
			}
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}					
	}	
}
