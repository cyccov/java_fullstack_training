package org.yamil.ms.consumo.vo;

public class MServicioVO {
	private String uri;
	private String method;
	
	public String getUri() {
		return uri;
	}
	public String getMethod() {
		return method;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	public void setMethod(String method) {
		this.method = method;
	}
}
