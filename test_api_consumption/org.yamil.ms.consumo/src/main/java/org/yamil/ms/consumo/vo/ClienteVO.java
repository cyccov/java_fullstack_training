package org.yamil.ms.consumo.vo;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public class ClienteVO implements Serializable{
	
	/**
	 * POJO class for Json
	 */
	private static final long serialVersionUID = 6529523044379503391L;
	private Long id;
	private String nombre;
	private String apellido;
	private String formaDePago;
	private Date creadoEn;
	private Date editadoEn; 
	
	//agregamos objeto de Lista para consumir arreglo de Clientes
	private List<ClienteVO> listaClientes;
	
	
	
	
	public List<ClienteVO> getListaClientes() {
		return listaClientes;
	}
	public void setListaClientes(List<ClienteVO> listaClientes) {
		this.listaClientes = listaClientes;
	}
	public Long getId() {
		return id;
	}
	public String getNombre() {
		return nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public String getFormaDePago() {
		return formaDePago;
	}
	public Date getCreadoEn() {
		return creadoEn;
	}
	public Date getEditadoEn() {
		return editadoEn;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public void setFormaDePago(String formaDePago) {
		this.formaDePago = formaDePago;
	}
	public void setCreadoEn(Date creadoEn) {
		this.creadoEn = creadoEn;
	}
	public void setEditadoEn(Date editadoEn) {
		this.editadoEn = editadoEn;
	}
	
	@Override
	public String toString() {
		return "id: " + getId()
				+ "nombre: " + getNombre()
				+ "apellido: " + getApellido()
				+ "forma de pago: " + getFormaDePago()
				+ "creado En: " + getCreadoEn().toString()
				+ "editadon en:" + getEditadoEn().toString();
	}
	
	
}
