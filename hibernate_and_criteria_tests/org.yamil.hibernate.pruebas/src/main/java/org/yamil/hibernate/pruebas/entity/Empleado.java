package org.yamil.hibernate.pruebas.entity;

import java.util.Date;

public class Empleado {
	
	private long idEmpleado;
	private String nombre;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String rfc;
	private Date fechaNacimiento;
	private Usuario usuario;
	
	
	public long getIdEmpleado() {
		return idEmpleado;
	}
	public String getNombre() {
		return nombre;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public String getRfc() {
		return rfc;
	}
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setIdEmpleado(long idEmpleado) {
		this.idEmpleado = idEmpleado;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}	
}
