package org.yamil.hibernate.pruebas.entity;

public class Employee {
	private long idEmployee;
	private String name;
	private float salary;
	private Address address;
	private Department department; //un depto tiene muchos empleados
	
	
	
	public Department getDepartment() {
		return department;
	}
	public void setDepartment(Department department) {
		this.department = department;
	}
	public long getIdEmployee() {
		return idEmployee;
	}
	public String getName() {
		return name;
	}
	public float getSalary() {
		return salary;
	}
	public Address getAddress() {
		return address;
	}
	public void setIdEmployee(long idEmployee) {
		this.idEmployee = idEmployee;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setSalary(float salary) {
		this.salary = salary;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
}
