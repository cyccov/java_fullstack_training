package org.yamil.hibernate.pruebas.app;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.yamil.hibernate.pruebas.entity.Employee;

public class ConsultasCriteria {

	public static void main(String[] args) {
		Session session = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory().openSession();
		Criteria criteria = session.createCriteria(Employee.class); //Definimos la entidad de donde hara las consultas...
		
		/**Select * from Employee*/
		List<Employee> listOfEmployees = criteria.list();
		
		//Iteramos la lista de resultados:
		for(Employee e : listOfEmployees) {
			System.out.println(e.getName());
			System.out.println(e.getIdEmployee());
			System.out.println(e.getSalary());
		}
		session = null;
		criteria = null;
//		session.close();
		

		session = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory().openSession();
		criteria = session.createCriteria(Employee.class); //Definimos la entidad de donde hara las consultas...
		System.out.println("SELECT * FROM EMPLOYEE LIKE NAME...");
		criteria.add(Restrictions.like("name","%Da%")); //el primer parametro es e atributo de la Entity..
		listOfEmployees = criteria.list(); //recuperamos los resultados
		
		for(Employee e : listOfEmployees) {
			System.out.println(e.getName());
			System.out.println(e.getIdEmployee());
			System.out.println(e.getSalary());
		}		
		
		session.close();
	}

}
