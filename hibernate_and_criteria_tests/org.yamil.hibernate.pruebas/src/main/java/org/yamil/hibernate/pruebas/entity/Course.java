package org.yamil.hibernate.pruebas.entity;

import java.util.Set;

public class Course {
	private long idCourse;
	private String courseName;
	private String duration;
	
	private Set<Student> students;

	public long getIdCourse() {
		return idCourse;
	}

	public String getCourseName() {
		return courseName;
	}

	public String getDuration() {
		return duration;
	}

	public Set<Student> getStudents() {
		return students;
	}

	public void setIdCourse(long idCourse) {
		this.idCourse = idCourse;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public void setStudents(Set<Student> students) {
		this.students = students;
	}
	
}
