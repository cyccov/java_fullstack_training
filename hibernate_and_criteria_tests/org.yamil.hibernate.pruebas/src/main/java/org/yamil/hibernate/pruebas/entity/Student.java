package org.yamil.hibernate.pruebas.entity;

import java.util.Set;

public class Student {
	private long idStudent;
	private String studentName;
	private int studentMark;
	
	private Set<Course> courses;

	public long getIdStudent() {
		return idStudent;
	}

	public String getStudentName() {
		return studentName;
	}

	public int getStudentMark() {
		return studentMark;
	}

	public Set<Course> getCourses() {
		return courses;
	}

	public void setIdStudent(long idStudent) {
		this.idStudent = idStudent;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public void setStudentMark(int studentMark) {
		this.studentMark = studentMark;
	}

	public void setCourses(Set<Course> courses) {
		this.courses = courses;
	}

	
	
	
}
