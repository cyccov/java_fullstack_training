package mx.com.yamil.hibernateapp;

import jakarta.persistence.EntityManager;
import mx.com.yamil.hibernateapp.entity.Cliente;
import mx.com.yamil.hibernateapp.entity.Factura;
import mx.com.yamil.hibernateapp.utilities.JpaUtilities;

public class HibernateAsociacionesOneToManyBidireccional {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EntityManager em = JpaUtilities.getEntityManager();
		try {
			em.getTransaction().begin();
			Cliente cliente = new Cliente("Cata", "edu");
			cliente.setFormaDePago("paypal");
			
			Factura f1 = new Factura("compras de super", 120l);
			Factura f2 = new Factura("verduras",1200L);
			//se hace la relacion bidireccional
			cliente.getFacturas().add(f1);
			cliente.getFacturas().add(f2);
			
			f1.setCliente(cliente);
			f2.setCliente(cliente);
			em.persist(cliente);
			em.getTransaction().commit();
			
			System.out.println(cliente);
		} catch(Exception e) {
			em.getTransaction().rollback();
			e.printStackTrace();
		} finally {
			em.close();
		}

	}

}
