package mx.com.yamil.hibernateapp;

import jakarta.persistence.EntityManager;
import mx.com.yamil.hibernateapp.entity.Cliente;
import mx.com.yamil.hibernateapp.entity.Factura;
import mx.com.yamil.hibernateapp.utilities.JpaUtilities;

public class HibernateAsociacionesOneToManyBidireccionalFind {
	public static void main(String[] args) {
		EntityManager em = JpaUtilities.getEntityManager();
		try {
			em.getTransaction().begin();
			Cliente cliente = em.find(Cliente.class, 1l);
			
			Factura f1 = new Factura("compras de super", 120l);
			Factura f2 = new Factura("verduras",1200L);
			//se hace la relacion bidireccional
			cliente.getFacturas().add(f1);
			cliente.getFacturas().add(f2);
			
			f1.setCliente(cliente);
			f2.setCliente(cliente);
//			em.merge(cliente);
			em.getTransaction().commit();
			
			System.out.println(cliente);
			//rliminar por ID
			em.getTransaction().begin();
			Factura f3 = em.find(Factura.class,1l);
			cliente.getFacturas().remove(f3);
			f3.setCliente(null);
			em.getTransaction().commit();
			System.out.println(cliente);

		} catch(Exception e) {
			em.getTransaction().rollback();
			e.printStackTrace();
		} finally {
			em.close();
		}
	}
}
