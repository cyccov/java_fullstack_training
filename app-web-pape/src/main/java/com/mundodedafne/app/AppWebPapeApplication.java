package com.mundodedafne.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppWebPapeApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppWebPapeApplication.class, args);
	}

}
