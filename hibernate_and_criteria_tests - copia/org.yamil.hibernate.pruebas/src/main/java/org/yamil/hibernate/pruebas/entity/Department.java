package org.yamil.hibernate.pruebas.entity;

import java.util.Set;

//many employees work at a department
public class Department {
	private long idDepartment;
	private String departmentName;
	private Set<Employee> employees; //un departmento tiene muchos empleados
	
	public long getIdDepartment() {
		return idDepartment;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public Set<Employee> getEmployees() {
		return employees;
	}
	public void setIdDepartment(long idDepartment) {
		this.idDepartment = idDepartment;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public void setEmployees(Set<Employee> employees) {
		this.employees = employees;
	}
	
	
	
}
