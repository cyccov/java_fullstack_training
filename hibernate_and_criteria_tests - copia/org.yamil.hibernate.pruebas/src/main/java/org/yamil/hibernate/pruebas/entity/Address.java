package org.yamil.hibernate.pruebas.entity;

public class Address {
	private long addressId;
	private String city;
	private String state;
	private String country;
	private Employee employee;
	
	public long getAddressId() {
		return addressId;
	}
	public String getCity() {
		return city;
	}
	public String getState() {
		return state;
	}
	public String getCountry() {
		return country;
	}
	public Employee getEmployee() {
		return employee;
	}
	public void setAddressId(long addressId) {
		this.addressId = addressId;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public void setState(String state) {
		this.state = state;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	
	
}
