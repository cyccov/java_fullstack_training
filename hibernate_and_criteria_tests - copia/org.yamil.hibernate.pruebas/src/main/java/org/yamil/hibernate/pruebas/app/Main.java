package org.yamil.hibernate.pruebas.app;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.yamil.hibernate.pruebas.entity.Address;
import org.yamil.hibernate.pruebas.entity.Course;
import org.yamil.hibernate.pruebas.entity.Department;
import org.yamil.hibernate.pruebas.entity.Employee;
import org.yamil.hibernate.pruebas.entity.Student;

public class Main {

	public static void main(String[] args) {
		Configuration config = new Configuration();
		config.configure("hibernate.cfg.xml");
		SessionFactory sessionFact = config.buildSessionFactory();
		Session session = sessionFact.openSession();
		Transaction transaction = session.beginTransaction();
		
		Employee employee = new Employee();
		employee.setName("Christian Yamiil");
		employee.setSalary(155.50f);
		
		Address address = new Address();
		address.setCity("Pachuca");
		address.setState("Hidalgo");
		address.setCountry("Mexico");
		
		employee.setAddress(address);
		address.setEmployee(employee);
		
		session.save(employee);
		transaction.commit();
		session.close();
		
		//---------------------------------------------
		System.out.println("ONE to ONE relationship: ");
		//---------------------------------------------
		
		System.out.println("Hibernate ONE to MANY Relationship:------------");
//		Session newSession = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory().openSession();
		session = sessionFact.openSession();
		transaction = session.beginTransaction();
		
		List<Employee> listaEmpleados;// = new ArrayList<Employee>();
		Employee aleida = new Employee();
		Employee yamil = new Employee();
		Employee dafne = new Employee();
		
		Address aleidaAddress = new Address();
		Address yamilAddress;
		Address dafneAddress;
		
		Department dept1 = new Department();

		aleida.setName("Aleida Torres Sanchez");
		aleida.setSalary(6550.75f);
		aleidaAddress.setCity("Pachuca");
		aleidaAddress.setCountry("Mexico");
		aleidaAddress.setState("Hidalgo");
		
		aleida.setAddress(aleidaAddress);
		aleidaAddress.setEmployee(aleida);
		yamilAddress = aleidaAddress;
		dafneAddress = aleidaAddress;
		
		yamil.setName("Yamil Castillo Covarrubias");
		yamil.setSalary(1.50f);
		yamilAddress.setEmployee(yamil);
		yamil.setAddress(yamilAddress);		
		
		dafne.setName("Dafne Castillo Torres");
		dafne.setSalary(0f);
		dafneAddress.setEmployee(dafne);
		dafne.setAddress(dafneAddress);		
		
		listaEmpleados = new ArrayList();
		listaEmpleados.add(dafne);
		listaEmpleados.add(aleida);
		listaEmpleados.add(yamil);
		
		Set<Employee> employees = new HashSet();
		employees.add(dafne);
		employees.add(aleida);
		employees.add(yamil);
		
		dept1.setDepartmentName("Computer Science");
		dept1.setEmployees(employees); //agrega los empleados que pertenecen al departmento CS
		
		session.save(dept1);
		transaction.commit();
		session.close();
		
		
		System.out.println("MANY TO ONE RELATIONSHIPS (Many Employees to one Department)");
		session = sessionFact.openSession();
		transaction = session.beginTransaction();
		
		Department hr = new Department();
		hr.setDepartmentName("H.R.");
		
		Employee empleado1 = new Employee();
		empleado1.setName("Juanito");
		empleado1.setDepartment(hr);
		
		Employee empleado2 = new Employee();
		empleado2.setName("Pedrito");
		empleado2.setDepartment(hr);
		
		session.save(empleado1);
		session.save(empleado2);
		transaction.commit();
		session.close();	
		
		//------------------------------------
		System.out.println("MANY TO MANY RELATIONSHIP: one course has many students and one student has many courses");
		session = sessionFact.openSession();
		transaction = session.beginTransaction();
		
		Student s1 = new Student();
		s1.setStudentName("Dafne Castillo Torres");
		s1.setStudentMark(90);
		
		Student s2 = new Student();
		s2.setStudentName("Alita de Pollo");
		s2.setStudentMark(90);
		
		Course c1 = new Course();
		c1.setCourseName("C.S. -- Java");
		c1.setDuration("1 Semester");
		
		Course c2 = new Course();
		c2.setCourseName("Calculus");
		c2.setDuration("1 semester");
		
		Set courses = new HashSet();
		courses.add(c1);
		courses.add(c2);
		
		s1.setCourses(courses);
		s2.setCourses(courses);
		
		session.save(s1);
		session.save(s2);
		
		transaction.commit();
		session.close();	
	}

}
