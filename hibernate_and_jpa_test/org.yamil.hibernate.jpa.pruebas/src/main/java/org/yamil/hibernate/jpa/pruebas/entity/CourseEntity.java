package org.yamil.hibernate.jpa.pruebas.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CourseEntity {
	
	//definimos entidad para que sea mapeada a base de datos
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "COURSE_ID")	
	private long idCourse;
	@Column(name = "COURSE_NAME")
	private String courseName;
	private String duration;
	
	public long getIdCourse() {
		return idCourse;
	}
	public String getCourseName() {
		return courseName;
	}
	public String getDuration() {
		return duration;
	}
	public void setIdCourse(long idCourse) {
		this.idCourse = idCourse;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	
	
}
