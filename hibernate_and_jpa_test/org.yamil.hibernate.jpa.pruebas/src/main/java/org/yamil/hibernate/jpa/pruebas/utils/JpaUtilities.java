package org.yamil.hibernate.jpa.pruebas.utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JpaUtilities {
	public EntityManager getEntityManager() {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("pruebaJPA");
		EntityManager manager = factory.createEntityManager();
		return manager;
	}
}
