package org.yamil.hibernate.jpa.pruebas.entity.app;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.yamil.hibernate.jpa.pruebas.entity.StudentEntity;
import org.yamil.hibernate.jpa.pruebas.utils.JpaUtilities;

public class Main{
//	Mas ejemplos: https://gitlab.com/cyccov/java_fullstack_training/-/tree/main/mx.com.yamil.hibernateapp.relaciones
//	https://gitlab.com/cyccov/java_fullstack_training/-/tree/main/mx.com.yamil.hibernateapp
	public static void main(String[] args) {
		JpaUtilities utils = new JpaUtilities();
		EntityManager manager = utils.getEntityManager();
		try {			
			System.out.println("INSERTANDO DATOS CON JPA2");
			StudentEntity student1 = new StudentEntity();
			student1.setStudentName("Christian_JPA");
			student1.setStudentMark(75);			
			manager.persist(student1);						
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e);
			manager.getTransaction().rollback();
		} finally {
			manager.close();
		}
	}
}
