package org.yamil.hibernate.jpa.pruebas.entity;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import javax.persistence.Entity;

@Entity
@Table(name = "T_STUDENT")
public class StudentEntity {
	
	//Se definen atributos para la tabla.
	@Id
	@Column(name = "STUDENT_ID")
	private long idStudent;
	@Column(name="ST_NAME")
	private String studentName;
	@Column(name="ST_MARK")
	private int studentMark;
	
	public long getIdStudent() {
		return idStudent;
	}
	public String getStudentName() {
		return studentName;
	}
	public int getStudentMark() {
		return studentMark;
	}
	public void setIdStudent(long idStudent) {
		this.idStudent = idStudent;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public void setStudentMark(int studentMark) {
		this.studentMark = studentMark;
	}
	
	
}
